set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Install Vundle Packages "
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"change surrounding"
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-repeat'
"NERD TREE"
Plugin 'scrooloose/nerdtree'
" AUTO SAVE"
Plugin '907th/vim-auto-save'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" NERD TREE "
" AUTO ON NO FILE"
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" AUTO ON DIR"
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

" Map NERDTREE "
map <C-n> :NERDTreeToggle<CR>

" Close VIM even if Nerdtree is open"
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

syntax on
filetype plugin indent on
set number
autocmd FileType gitcommit setlocal spell
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
:imap jj <Esc>
set visualbell
set noerrorbells
set autoindent
map <C-n> :NERDTreeToggle<CR>
" AUTO SAVE SETTINGS "
let g:auto_save = 1
let g:auto_save_events = ["InsertLeave", "TextChanged"]
