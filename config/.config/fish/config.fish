set fish_greeting

# Add doom
fish_add_path ~/.emacs.d/bin
# Add pyenv
fish_add_path ~/.pyenv/bin
# pyenv init
status is-interactive; and pyenv init --path | source
pyenv init - | source
